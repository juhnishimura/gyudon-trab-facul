import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import PaginaPrincipal from './paginaPrincipal';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <PaginaPrincipal />
  </React.StrictMode>,
  document.getElementById('root')
);


reportWebVitals();
