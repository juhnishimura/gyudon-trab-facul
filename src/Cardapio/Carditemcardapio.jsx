import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMapMarked,
  faMapMarker,
  faPhone,
} from "@fortawesome/free-solid-svg-icons";
import style from "./Cardapio.module.scss";
function Carditemcardapio(props) {
  return (
    <>
      <div className={props.col}>
        <div className={style.cardCardapio}>
          <img src={props.imagem} />
          <div className={style.dadosCardapio}>
            <label>{props.titulo}</label>
            <span>{props.descricao}</span>
            <label className={style.preco} >{props.preco}</label>
          </div>
        </div>
      </div>
    </>
  );
}

export default Carditemcardapio;
