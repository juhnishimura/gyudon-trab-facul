import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMapMarked,
  faMapMarker,
  faPhone,
} from "@fortawesome/free-solid-svg-icons";
import style from "./Cardapio.module.scss";
import Carditemcardapio from "./Carditemcardapio";

function Cardapio() {
  return (
    <>
      <div className={style.cardapio}>

      <div className={style.aviso}>
          <h4>
            Trabalhamos apenas em <span>Campinas</span> e{" "}
            <span>por encomenda</span> por enquanto
          </h4>
        </div>
       

        <h3>Cardápio</h3>
        <div className={style.botoesCardapio}>
          <button>Gyudon</button>
          <button>Bentôs</button>
          <button>Karês</button>
          <button>Porções</button>
        </div>
        <div className={style.itensCardapio}>
          <div>
            <div className={style.rowCardapio}>
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
            </div>
          </div>
          <div >
          <div className={style.rowCardapio}>
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
            </div>
          </div>
          <div >
          <div className={style.rowCardapio}>
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
            </div>
          </div>
          <div className={style.rowCardapio}>
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
              <Carditemcardapio
                titulo="Bento Gyudon"
                descricao="Uma deliciosa combinação de arroz japones de carne ao molho japonês"
                preco="R$ 20,00 "
              />
            </div>
        </div>
      </div>
    </>
  );
}

export default Cardapio;
