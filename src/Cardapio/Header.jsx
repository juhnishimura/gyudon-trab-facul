import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarked, faMapMarker, faPhone } from "@fortawesome/free-solid-svg-icons";
import style from './Header.module.scss'
function Header() {
  return (
    <>
        
        <div className={style.Header} >
          <div>
            <h4 className={style.localizacao}><FontAwesomeIcon color="#FFB01A "  icon={faMapMarked} /> <span>Rua Alberto de Oliveira Maia, Campinas - SP</span></h4>
          </div>
          <div>
          <h4 className={style.number}><FontAwesomeIcon color="#FFB01A "  icon={faPhone} /> <span  >(19)97123 - 9003</span></h4>
          </div>
        </div>
    </>
  );
}

export default Header;
