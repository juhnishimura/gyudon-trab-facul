import React from "react";
import Header from "./Header/Header";
import Body from "./Body/Body";
import Cardapio from "./Cardapio/Cardapio";

function PaginaPrincipal() {
  return (
    <>
    <Header/>
    <Body/>
     
    <Cardapio/>
    </>
  );
}

export default PaginaPrincipal;
