import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarked, faMapMarker, faPhone } from "@fortawesome/free-solid-svg-icons";
import style from './Body.module.scss'
import imagemFundo from './gyudon-foto.jpg';
import logo from './logo.png'


function Body() {
    return (
      <>
      <div className={style.Body}>
          <div className={style.logo}>
            <div className={style.menu}>
              <button>Início</button>
              <button>Cardápio</button>
              <button>Menu</button>
            </div>
          <img src={logo}></img>
          </div>
          
          <img className={style.fundo} src={imagemFundo}></img>
      </div>
      </>
    );
  }
  
  export default Body;
  